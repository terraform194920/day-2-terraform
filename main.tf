terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
    region = "ap-southeast-1"
}

#elasticIp for nat
resource "aws_eip" "elastic_ip" {
  domain = "vpc"

}

data "aws_key_pair" "mykey" {
  key_name           = "test"
  include_public_key = true
}

output "fingerprint" {
  value = data.aws_key_pair.mykey.fingerprint
}

output "name" {
  value = data.aws_key_pair.mykey.key_name
}

output "id" {
  value = data.aws_key_pair.mykey.id
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.8.1"
  name = "my-vpc"
  cidr = "10.0.0.0/16"
  azs             = ["ap-southeast-1a"]
  private_subnets = ["10.0.2.0/24"]
  public_subnets  = ["10.0.1.0/24"]

# define NAT Gateway and associate eip
  enable_nat_gateway = true
  single_nat_gateway = true
  one_nat_gateway_per_az = false
  reuse_nat_ips       = true 
  external_nat_ip_ids = "${aws_eip.elastic_ip.*.id}"
  create_igw = true

}
resource "aws_security_group" "sg" {
    vpc_id = module.vpc.vpc_id
    name = "terraform-SG"
}
resource "aws_security_group_rule" "publ_out" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.sg.id

}
resource "aws_security_group_rule" "public_in" {
  type        = "ingress"
  from_port   = 0
  to_port     = 65535
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.sg.id
}

#create instance
locals {
  instances = {
    "public-instance" = {
      subnet_id                   = module.vpc.public_subnets
      associate_public_ip_address = true
    },
    "private-instance" = {
      subnet_id                   = module.vpc.private_subnets
      associate_public_ip_address = true
    }
  }
}
resource "aws_instance" "ec2_instance" {
  for_each       = local.instances
  ami            = "ami-05b46bc4327cf9d99"
  instance_type  = "t2.micro"
  key_name       = data.aws_key_pair.mykey.key_name
  subnet_id      = element(each.value["subnet_id"], 0)  
  associate_public_ip_address = each.value["associate_public_ip_address"] 
  vpc_security_group_ids = [aws_security_group.sg.id]
  tags = {
    Name = each.key
  }
}
